#pragma once
#pragma warning(disable : 4996)

#include <string.h>
#include <stdlib.h>
#include <ctype.h>

char* get_fname(const char* fname, const char* ext);
int stoi(char *str);
char* trim(char *str);
int strnchr(const char* str1, const int chr_find);
char* itom(int c, char buf[]);
char *zStrtok(char *str, const char *delim);