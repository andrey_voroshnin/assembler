#include "assembler.h"

int main(int argc, char *argv[])
{
	int i = 1;
	char *fname = NULL;
	char *err_fname = NULL;
	FILE *err_file = NULL;
	char buffer[MAX_STR_SIZE];
	int pass = 0;
	int errorsP = 0;
	int errorsW = 0;

	if (argc < 2) {
		printf("No input files specified\n");
		return 0;
	}

	for (i = 1; i < argc; i++) {
		if (strchr(argv[i], '.') != NULL) {
			fprintf(err_file, "Wrong file name %s, please pass file without extension\n", argv[i]);
			return 0;
		}
		fname = argv[i];
		err_fname = (char*)malloc((strlen(fname) * sizeof(char)) + sizeof(".err"));
		if (!err_fname || !fname) {
			printf("Memmory allocation error\n");
			return 0;
		}
		strcpy(err_fname, fname);
		strcat(err_fname, ".err");
		err_file = fopen(err_fname, "w");
		if (!err_file) {
			printf("Cannot create//open files for writing\n");
			free(err_fname);
			return 0;
		}
		pass = 0;
		errorsP = 0;
		while (pass < 2) {
			++pass;
			if (ftell(err_file) || !read_file(fname, err_file)) {
				++errorsP;
				break;
			}
			if (!errorsP && pass > 1)
				errorsW += write_file(fname, err_file);
		}
		free(err_fname);
		fclose(err_file);
	}

	if (errorsP || errorsW) {
		for (i = 1; i < argc; i++) {
			err_fname = (char*)malloc((strlen(argv[i]) * sizeof(char)) + sizeof(".err"));
			if (!err_fname) {
				printf("Memory allocation error\n");
				return 0;
			}
			strcpy(err_fname, argv[i]);
			strcat(err_fname, ".err");
			err_file = fopen(err_fname, "r");
			if (!err_file) {
				printf("Cannot create//open files for writing\n");
				free(err_fname);
				return 0;
			}
			fseek(err_file, 0, SEEK_END);
			if (!ftell(err_file)) {
				fclose(err_file);
				free(err_fname);
				continue;
			}
			fseek(err_file, 0, SEEK_SET);
			printf("\nPrinting compilations errors for file %s:\n", argv[i]);
			while (fgets(buffer, sizeof(buffer), err_file))
				printf("%s", buffer);
			strcpy(buffer, "find . / -maxdepth 1 -name \"");
			strcat(buffer, argv[i]);
			strcat(buffer, ".*\" -not -name \"");
			strcat(buffer, argv[i]);
			strcat(buffer, ".as\" -exec rm -f {} \\;");
			system(buffer);
			fclose(err_file);
			free(err_fname);
		}
	}

	system("rm -f *.err");
	return 1;
}