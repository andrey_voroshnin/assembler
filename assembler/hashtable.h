#pragma once

#include <string.h>
#include <stdlib.h>

#define SIZE 30

int search_key(const char* key);
int search_type(const char* key);
int insert(const char* key, int data, int type);
/* void display(); */
int update(const char* key, int data);
int delete(const char* key);
int search_key_ext(const char* key);
int delete_key_ext(const char* key);
