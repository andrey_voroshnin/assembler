#include "utils.h"

/* Find first character occurence and return index */
int strnchr(const char* str1, const int chr_find)
{
	int i = 0;
	while (str1[i] != '\0') {
		if (str1[i] == chr_find)
			return i;
		else
			i++;
	}

	return -1;
}

/* strtok implementation. Works with consecutive tokens */
char *zStrtok(char *str, const char *delim)
{
	static char *static_str = NULL;      /* var to store last address */
	int index = 0, strlength = 0;           /* integers for indexes */
	int found = 0;                  /* check if delim is found */

	/* delimiter cannot be NULL	if no more char left, return NULL as well */
	if (delim == NULL || (str == NULL && static_str == NULL))
		return NULL;

	if (str == NULL)
		str = static_str;

	/* get length of string */
	strlength = (int)strlen(str);

	/* find the first occurance of delim */
	for (index = 0; index < strlength; index++)
		if (str[index] == delim[0]) {
			found = 1;
			break;
		}

	/* if delim is not contained in str, return str */
	if (!found) {
		static_str = NULL;
		return str;
	}

	/* check for consecutive delimiters if first char is delim, return delim */
	if (str[0] == delim[0]) {
		static_str = (str + 1);
		return (char *)delim;
	}

	/* terminate the string this assignmetn requires char[], so str has to be char[] rather than *char */
	str[index] = '\0';

	/* save the rest of the string */
	if ((str + index + 1) != NULL)
		static_str = (str + index + 1);
	else
		static_str = NULL;

	return str;
}

/* trim leading and trailing spaces */
char* trim(char *str)
{
	char *end = NULL;

	/* Trim leading space */
	while (isspace(*str)) str++;

	if (*str == '\0')  /* All spaces? */
		return str;

	/* Trim trailing space */
	end = str + strlen(str) - 1;
	while (end > str && isspace(*end)) end--;

	/*  Write new null terminator */
	*(end + 1) = '\0';

	return str;
}

/* Convert string to integer and reset all digits except first 10 */
int stoi(char *str)
{
	int result = 0;
	int sign = 1;

	str = trim(str);

	if (*str == '\0')
		return 0;
	if (*str == '-') { /* Handle sign */
		sign = sign * -1;
		str++;
	}
	else if (*str == '+')
		str++;
	while (*str != '\0') {
		if ((*str >= '0') && (*str <= '9')) {
			result = (result * 10) + ((*str) - '0');
			str++;
		}
		else
			return 0;
	}
	return (1023 & (result * sign));
}

/* Create filename with extension. Free pointer after use */
char* get_fname(const char* fname, const char* ext)
{
	char* temp_str = NULL;

	temp_str = (char*)malloc((strlen(fname) * sizeof(char)) + (strlen(ext) * sizeof(char)) + 1);
	strcpy(temp_str, fname);
	strcat(temp_str, ext);

	return temp_str;
}

/* Integer to base 32 (muzar). Not versatile because we need only 10 digits */
char* itom(int c, char buf[])
{
	char muzar[32] = "!@#$%^&*<>abcdefghijklmnopqrstuv";
	c = 1023 & c; /* Reset all digits except first 10 */
	buf[1] = muzar[c % 32];
	c = c / 32;
	buf[0] = muzar[c % 32];
	buf[2] = '\0';
	return buf;
}