#pragma warning(disable : 4996)

#include "parser.h"

int IC = 0;
int IC_old = 0;
int DC = 0;
int L = 0;
int lineN = 0;
int pass = 0;
int DataTable[MAX_DATA];
int OperTable[MAX_DATA];
char *asm_oper[] = ASM_CMD;
char *registers[] = REGISTERS;
char *work_fname;

int check_label(const char* label)
{
	int i = 0;
	if (!isalpha(label[0]))
		return 0;
	while (label[i] != '\0') {
		if (!isalnum(label[i]))
			return 0;
		i++;
	}
	if (i > LBL_SIZE)
		return 0;
	return 1;
}

int get_var_type(const char* var)
{
	if (strnchr(var, '#') != -1)
		return 0;
	if (strnchr(var, '.') != -1)
		return 2;
	if (check_register(var) != -1)
		return 3;
	return 1;
}

int check_register(const char* reg)
{
	int i = 0;

	for (i = 0; i < REGISTER_NUM; i++)
		if (strstr(reg, registers[i]))
			return i;
	return -1;
}

int read_var(char* var, const int type)
{
	int reg = 0;
	char lbl[LBL_SIZE];
	var = trim(var);
	OperTable[IC] <<= 2;
	OperTable[IC] += type;
	++L;

	switch (type)
	{
	case 0:
		if (pass == 2)
			break;
		++var;
		OperTable[IC + L] = (stoi(var) << 2);
		if (!OperTable[IC + L])
			return -1;
		break;
	case 1:
		if (!check_label(var))
			return -1;
		if (search_type(var) == 5 && pass == 1) {
			OperTable[IC + L] = 1;
			insert(var, IC + 100 + L, 5);
		}
		else if (search_type(var) != 5 && search_type(var) != -1 && pass == 2) {
			OperTable[IC + L] = search_key(var) + IC_old + 100;
			OperTable[IC + L] = (OperTable[IC + L] << 2) + 2;
		}
		break;
	case 2:
		memset(lbl, '\0', LBL_SIZE);
		strncpy(lbl, var, strnchr(var, '.'));
		if (!check_label(lbl))
			return -1;
		if (pass == 2) {
			OperTable[IC + L] = search_key(lbl) + IC_old + 100;
			OperTable[IC + L] = (OperTable[IC + L] << 2) + 2;
		}
		++L;
		var += strnchr(var, '.') + 1;
		if (stoi(var) > 2 || stoi(var) < 1)
			return -1;
		OperTable[IC + L] = (stoi(var) << 2);
		break;
	case 3:
		if (pass == 2)
			break;
		reg = check_register(var);
		if (reg == -1)
			return -1;
		if (OperTable[IC + L] == 0) {
			OperTable[IC + L] += reg;
			OperTable[IC + L] <<= 2;
		}
		else {
			OperTable[IC + L] <<= 2;
			OperTable[IC + L] += reg;
			OperTable[IC + L] <<= 2;
		}
		break;
	default:
		break;
	}
	return type;
}

int read_struct(const char* label, char* line, FILE *err_file)
{
	char* ret;
	int val = 0;
	char token[] = ",";
	line = trim(line);

	if (DC >= MAX_DATA) {
		fprintf(err_file, "Data table exhausted\n");
		return 0;
	}
	insert(label, DC, 3);
	line = strstr(line, ".struct") + 7;
	line = trim(line);
	ret = zStrtok(line, token);
	val = stoi(ret);
	if (!val) {
		fprintf(err_file, "Line %d: Wrong number declaration\n", lineN);
		return 0;
	}
	DataTable[DC] = val;
	++DC;
	ret = zStrtok(NULL, token);
	ret += strnchr(ret, '"') + 1;
	while (*ret != '"' && *ret != '\0') {
		if (DC >= MAX_DATA) {
			fprintf(err_file, "Data table exhausted\n");
			return 0;
		}
		DataTable[DC] = *ret;
		++DC;
		++ret;
	}
	DataTable[DC] = 0;
	++DC;
	return 1;
}

int read_string(const char* label, char* line, FILE *err_file)
{
	trim(line);

	insert(label, DC, 2);
	line += strnchr(line, '"') + 1;
	while (*line != '"' && *line != '\0') {
		if (DC >= MAX_DATA) {
			fprintf(err_file, "Data table exhausted\n");
			return 0;
		}
		DataTable[DC] = *line;
		++DC;
		++line;
	}
	DataTable[DC] = 0;
	++DC;
	return 1;
}

int read_int(const char* label, char* line, FILE *err_file)
{
	char* ret;
	int val = 0;
	char token[] = ",";
	line = trim(line);

	if (DC >= MAX_DATA) {
		fprintf(err_file, "Data table exhausted\n");
		return 0;
	}
	insert(label, DC, 1);
	line = strstr(line, ".data") + 5;
	line = trim(line);
	ret = zStrtok(line, token);
	while (ret != NULL) {
		if (DC >= MAX_DATA) {
			fprintf(err_file, "Data table exhausted\n");
			return 0;
		}
		val = stoi(ret);
		if (!val) {
			fprintf(err_file, "Line %d: Wrong number declaration\n", lineN);
			return 0;
		}
		DataTable[DC] = val;
		++DC;
		ret = zStrtok(NULL, token);
	}
	return 1;
}

void read_operation(char* line, FILE *err_file)
{
	int count, type, regN, period_flag, hash_flag;
	char* tok1 = NULL;
	char token[] = ",";
	unsigned int i = 0;
	line = trim(line);
	L = 0;
	count = type = regN = period_flag = hash_flag = 0;

	for (i = 0; i < ASM_CMD_NUM; i++) {
		if (!strncmp(line, asm_oper[i], strlen(asm_oper[i]))) {
			line += strlen(asm_oper[i]);
			OperTable[IC] = i;
			break;
		}
	}

	line = trim(line);
	period_flag = strnchr(line, ',');
	hash_flag = strnchr(line, '#');

	switch (i)
	{
	case 0: /* mov */
		if (*line == '\0')
			fprintf(err_file, "Line %d: 'mov' no variable specified\n", lineN);
		else if (period_flag < 0)
			fprintf(err_file, "Line %d: 'mov' must have 2 parameters separated by ','\n", lineN);
		else {
			tok1 = zStrtok(line, token);
			while (tok1 != NULL) {
				count++;
				type = get_var_type(tok1);
				if (count > 2) {
					fprintf(err_file, "Line %d: 'mov' too much parameters\n", lineN);
					return;
				}
				if (type == 3) {
					++regN;
					if (regN > 1)
						--L;
				}
				type = read_var(tok1, type);
				if (type == -1) {
					fprintf(err_file, "Line %d: 'mov' syntax error\n", lineN);
					return;
				}
				if (type == 0 && count > 1) {
					fprintf(err_file, "Line %d: 'mov' destination must be variable. No direct numbers\n", lineN);
					return;
				}
				if (pass == 1) {
					if (type == 2 && regN == 1 && count > 1)
						OperTable[IC + L - 2] <<= 4;
					else if (regN == 1 && type != 3 && count > 1)
						OperTable[IC + L - 1] <<= 4;
				}
				tok1 = zStrtok(NULL, token);
			}
		}
		break;
	case 1: /* cmp */
		if (*line == '\0')
			fprintf(err_file, "Line %d: 'cmp' no variable specified\n", lineN);
		else if (period_flag < 0)
			fprintf(err_file, "Line %d: 'cmp' must have 2 parameters separated by ','\n", lineN);
		else {
			tok1 = zStrtok(line, token);
			while (tok1 != NULL) {
				count++;
				type = get_var_type(tok1);
				if (count > 2) {
					fprintf(err_file, "Line %d: 'cmp' too much parameters\n", lineN);
					return;
				}
				if (type == 3) {
					++regN;
					if (regN > 1)
						--L;
				}
				type = read_var(tok1, type);
				if (type == -1) {
					fprintf(err_file, "Line %d: 'cmp' syntax error\n", lineN);
					return;
				}
				if (pass == 1) {
					if (type == 2 && regN == 1 && count > 1)
						OperTable[IC + L - 2] <<= 4;
					else if (regN == 1 && type != 3 && count > 1)
						OperTable[IC + L - 1] <<= 4;
				}
				tok1 = zStrtok(NULL, token);
			}
		}
		break;
	case 2: /* add */
		if (*line == '\0')
			fprintf(err_file, "Line %d: 'add' no variable specified\n", lineN);
		else if (period_flag < 0)
			fprintf(err_file, "Line %d: 'add' must have 2 parameters separated by ','\n", lineN);
		else {
			tok1 = zStrtok(line, token);
			while (tok1 != NULL) {
				count++;
				type = get_var_type(tok1);
				if (count > 2) {
					fprintf(err_file, "Line %d: 'add' too much parameters\n", lineN);
					return;
				}
				if (type == 3) {
					++regN;
					if (regN > 1)
						--L;
				}
				type = read_var(tok1, type);
				if (type == -1) {
					fprintf(err_file, "Line %d: 'add' syntax error\n", lineN);
					return;
				}
				if (type == 0 && count > 1) {
					fprintf(err_file, "Line %d: 'add' destination must be variable. No direct numbers\n", lineN);
					return;
				}
				if (pass == 1) {
					if (type == 2 && regN == 1 && count > 1)
						OperTable[IC + L - 2] <<= 4;
					else if (regN == 1 && type != 3 && count > 1)
						OperTable[IC + L - 1] <<= 4;
				}
				tok1 = zStrtok(NULL, token);
			}
		}
		break;
	case 3: /* sub */
		if (*line == '\0')
			fprintf(err_file, "Line %d: 'sub' no variable specified\n", lineN);
		else if (period_flag < 0)
			fprintf(err_file, "Line %d: 'sub' must have 2 parameters separated by ','\n", lineN);
		else {
			tok1 = zStrtok(line, token);
			while (tok1 != NULL) {
				count++;
				type = get_var_type(tok1);
				if (count > 2) {
					fprintf(err_file, "Line %d: 'sub' too much parameters\n", lineN);
					return;
				}
				if (type == 3) {
					++regN;
					if (regN > 1)
						--L;
				}
				type = read_var(tok1, type);
				if (type == -1) {
					fprintf(err_file, "Line %d: 'sub' syntax error\n", lineN);
					return;
				}
				if (type == 0 && count > 1) {
					fprintf(err_file, "Line %d: 'sub' destination must be variable. No direct numbers\n", lineN);
					return;
				}
				if (pass == 1) {
					if (type == 2 && regN == 1 && count > 1)
						OperTable[IC + L - 2] <<= 4;
					else if (regN == 1 && type != 3 && count > 1)
						OperTable[IC + L - 1] <<= 4;
				}
				tok1 = zStrtok(NULL, token);
			}
		}
		break;
	case 4: /* not */
		OperTable[IC] <<= 2;
		if (*line == '\0')
			fprintf(err_file, "Line %d: 'not' no variable specified\n", lineN);
		else if (period_flag >= 0)
			fprintf(err_file, "Line %d: 'not' only 1 parameter allowed\n", lineN);
		else if (hash_flag >= 0)
			fprintf(err_file, "Line %d: 'not' only variables allowed. No direct numbers\n", lineN);
		else {
			type = get_var_type(line);
			type = read_var(line, type);
			if (type == -1) {
				fprintf(err_file, "Line %d: 'not' syntax error\n", lineN);
				return;
			}
		}
		break;
	case 5: /* clr */
		OperTable[IC] <<= 2;
		if (*line == '\0')
			fprintf(err_file, "Line %d: 'clr' no variable specified\n", lineN);
		else if (period_flag >= 0)
			fprintf(err_file, "Line %d: 'clr' only 1 parameter allowed\n", lineN);
		else if (hash_flag >= 0)
			fprintf(err_file, "Line %d: 'clr' only variables allowed. No direct numbers\n", lineN);
		else {
			type = get_var_type(line);
			type = read_var(line, type);
			if (type == -1) {
				fprintf(err_file, "Line %d: 'clr' syntax error\n", lineN);
				return;
			}
		}
		break;
	case 6: /* lea */
		if (*line == '\0')
			fprintf(err_file, "Line %d: 'lea' no variable specified\n", lineN);
		else if (period_flag < 0)
			fprintf(err_file, "Line %d: 'lea' must have 2 parameters separated by ','\n", lineN);
		else {
			tok1 = zStrtok(line, token);
			while (tok1 != NULL) {
				count++;
				type = get_var_type(tok1);
				if (count > 2) {
					fprintf(err_file, "Line %d: 'lea' too much parameters\n", lineN);
					return;
				}
				type = read_var(tok1, type);
				if (type == -1) {
					fprintf(err_file, "Line %d: 'lea' syntax error\n", lineN);
					return;
				}
				if (type == 0) {
					fprintf(err_file, "Line %d: 'lea' only labels allowed. No direct numbers\n", lineN);
					return;
				}
				if (type == 2 && count > 1) {
					fprintf(err_file, "Line %d: 'lea' only labels allowed as destination. No registers\n", lineN);
					return;
				}
				tok1 = zStrtok(NULL, token);
			}
		}
		break;
	case 7: /* inc */
		OperTable[IC] <<= 2;
		if (*line == '\0')
			fprintf(err_file, "Line %d: 'inc' no variable specified\n", lineN);
		else if (period_flag >= 0)
			fprintf(err_file, "Line %d: 'inc' only 1 parameter allowed\n", lineN);
		else if (hash_flag >= 0)
			fprintf(err_file, "Line %d: 'inc' only variables allowed. No direct numbers\n", lineN);
		else {
			type = get_var_type(line);
			type = read_var(line, type);
			if (type == -1) {
				fprintf(err_file, "Line %d: 'inc' syntax error\n", lineN);
				return;
			}
		}
		break;
	case 8: /* dec */
		OperTable[IC] <<= 2;
		if (*line == '\0')
			fprintf(err_file, "Line %d: 'dec' no variable specified\n", lineN);
		else if (period_flag >= 0)
			fprintf(err_file, "Line %d: 'dec' only 1 parameter allowed\n", lineN);
		else if (hash_flag >= 0)
			fprintf(err_file, "Line %d: 'dec' only variables allowed. No direct numbers\n", lineN);
		else {
			type = get_var_type(line);
			type = read_var(line, type);
			if (type == -1) {
				fprintf(err_file, "Line %d: 'dec' syntax error\n", lineN);
				return;
			}
		}
		break;
	case 9: /* jmp */
		OperTable[IC] <<= 2;
		if (*line == '\0')
			fprintf(err_file, "Line %d: 'jmp' no variable specified\n", lineN);
		else if (period_flag >= 0)
			fprintf(err_file, "Line %d: 'jmp' only 1 parameter allowed\n", lineN);
		else if (hash_flag >= 0)
			fprintf(err_file, "Line %d: 'jmp' only variables allowed. No direct numbers\n", lineN);
		else {
			type = get_var_type(line);
			type = read_var(line, type);
			if (type == -1) {
				fprintf(err_file, "Line %d: 'jmp' syntax error\n", lineN);
				return;
			}
		}
		break;
	case 10: /* bne */
		OperTable[IC] <<= 2;
		if (*line == '\0')
			fprintf(err_file, "Line %d: 'bne' no variable specified\n", lineN);
		else if (period_flag >= 0)
			fprintf(err_file, "Line %d: 'bne' only 1 parameter allowed\n", lineN);
		else if (hash_flag >= 0)
			fprintf(err_file, "Line %d: 'bne' only variables allowed. No direct numbers\n", lineN);
		else {
			type = get_var_type(line);
			type = read_var(line, type);
			if (type == -1) {
				fprintf(err_file, "Line %d: 'bne' syntax error\n", lineN);
				return;
			}
		}
		break;
	case 11: /* red */
		OperTable[IC] <<= 2;
		if (*line == '\0')
			fprintf(err_file, "Line %d: 'red' no variable specified\n", lineN);
		else if (period_flag >= 0)
			fprintf(err_file, "Line %d: 'red' only 1 parameter allowed\n", lineN);
		else if (hash_flag >= 0)
			fprintf(err_file, "Line %d: 'red' only variables allowed. No direct numbers\n", lineN);
		else {
			type = get_var_type(line);
			type = read_var(line, type);
			if (type == -1) {
				fprintf(err_file, "Line %d: 'red' syntax error\n", lineN);
				return;
			}
		}
		break;
	case 12: /* prn */
		OperTable[IC] <<= 2;
		if (*line == '\0')
			fprintf(err_file, "Line %d: 'prn' no variable specified\n", lineN);
		else if (period_flag >= 0)
			fprintf(err_file, "Line %d: 'prn' only 1 parameter allowed\n", lineN);
		else {
			type = get_var_type(line);
			type = read_var(line, type);
			if (type == -1) {
				fprintf(err_file, "Line %d: 'prn' syntax error\n", lineN);
				return;
			}
		}
		break;
	case 13: /* jsr */
		OperTable[IC] <<= 2;
		if (*line == '\0')
			fprintf(err_file, "Line %d: 'jsr' no variable specified\n", lineN);
		else if (period_flag >= 0)
			fprintf(err_file, "Line %d: 'jsr' only 1 parameter allowed\n", lineN);
		else if (hash_flag >= 0)
			fprintf(err_file, "Line %d: 'jsr' only variables allowed. No direct numbers\n", lineN);
		else {
			type = get_var_type(line);
			type = read_var(line, type);
			if (type == -1) {
				fprintf(err_file, "Line %d: 'jsr' syntax error\n", lineN);
				return;
			}
		}
		break;
	case 14: /* rts */
		if (*line != '\0')
			fprintf(err_file, "Line %d: 'rts' parameters allowed\n", lineN);
		OperTable[IC] <<= 4;
		break;
	case 15: /* stop */
		if (*line != '\0')
			fprintf(err_file, "Line %d: 'stop' no parameters allowed\n", lineN);
		OperTable[IC] <<= 4;
		break;
	default:
		fprintf(err_file, "Line %d: Illegal command name\n", lineN);
		break;
	}
	OperTable[IC] <<= 2;
	++IC;
	IC += L;
	return;
}

int read_extern(const char* ext, const char* ext_fname, FILE *err_file)
{
	FILE *fp;
	char buf[3];
	int key = 0;

	if (search_key(ext) != -100)
		insert(ext, -100, 5);

	if (pass == 2) {
		fp = fopen(ext_fname, "a");
		if (fp == NULL) {
			fprintf(err_file, "Cannot open file %s for writing\n", ext_fname);
			return 0;
		}
		while ((key = search_key_ext(ext)) != -1) {
			if (key >= 0)
				fprintf(fp, "%s\t%s\n", ext, itom(key, buf));
			delete_key_ext(ext);
		}
		fclose(fp);
	}
	return 1;
}

int read_entry(const char* entry, char* ent_fname, FILE *err_file)
{
	FILE *fp;
	char buf[3];
	int key = 0;

	fp = fopen(ent_fname, "a");
	if (fp == NULL) {
		fprintf(err_file, "Cannot open file %s for writing\n", ent_fname);
		return 0;
	}
	if (search_type(entry) < 4)
		key = search_key(entry) + IC_old + 100;
	else
		key = search_key(entry);
	fprintf(fp, "%s\t%s\n", entry, itom(key, buf));
	fclose(fp);
	return 1;
}

void reset_globals(const char* fname)
{
	int i = 0;
	IC = DC = IC_old = lineN = pass = 0;
	for (i = 0; i < MAX_DATA; i++)
		OperTable[i] = DataTable[i] = 0;
	delete(NULL);
	free(work_fname);
	work_fname = (char*)malloc(strlen(fname) * sizeof(char) + 1);
	if (!work_fname)
		return;
	work_fname = strcpy(work_fname, fname);
}

char* reset_buffer()
{
	static char *tmp;

	free(tmp);
	tmp = (char*)malloc(sizeof(char)*STR_SIZE);
	if (!tmp)
	{
		printf("Memory allocation error.\n");
		exit(0);
	}
	return tmp;
}

int read_file(const char* fname, FILE *err_file)
{
	FILE *fp;
	char label[LBL_SIZE];
	char *as_fname, *ext_fname, *ent_fname, *buffer, *tmp;
	int flag_lbl;
	flag_lbl = IC = lineN = 0;
	buffer = tmp = as_fname = ext_fname = ent_fname = NULL;

	if (pass > 2 || !work_fname) {
		work_fname = (char*)malloc(strlen(fname) * sizeof(char) + 1);
		if (!work_fname)
			return 0;
		work_fname = strcpy(work_fname, fname);
	}

	if (strcmp(fname, work_fname))
		reset_globals(fname);

	if (!(as_fname = get_fname(fname, ".as")) || !(ext_fname = get_fname(fname, ".ext")) || !(ent_fname = get_fname(fname, ".ent")))
		return 0;

	pass++;
	fp = fopen(as_fname, "r");
	if (!fp) {
		fprintf(err_file, "Cannot open file %s for reading or file doesn't exists\n", fname);
		return 0;
	}

	buffer = reset_buffer();
	while (fgets(buffer, STR_SIZE, fp))
	{
		lineN++;
		buffer = trim(buffer);
		if (*buffer == ';' || *buffer == '\0')
			continue;
		else if (*buffer == '.')
		{
			if (strstr(buffer, ".extern")) {
				buffer += sizeof(".extern");
				buffer = trim(buffer);
				if (!read_extern(buffer, ext_fname, err_file))
					return 0;
			}
			else if (strstr(buffer, ".entry")) {
				if (pass == 2) {
					buffer += sizeof(".entry");
					buffer = trim(buffer);
					if (!read_entry(buffer, ent_fname, err_file))
						return 0;
				}
			}
			else {
				fprintf(err_file, "Line %d: General syntax error. Wrong variable declaration\n", lineN);
			}
		}
		else if ((flag_lbl = strnchr(buffer, ':')) == 0 )
			fprintf(err_file, "Line %d: Wrong label declaration\n", lineN);
		else if (flag_lbl != -1) {
			
			if (strnchr(buffer, '"') != -1)
				if (flag_lbl > strnchr(buffer, '"')) {
					read_operation(buffer, err_file);
					buffer = reset_buffer();
					continue;
				}
			memset(label, '\0', LBL_SIZE);
			memcpy(label, buffer, flag_lbl);
			buffer += flag_lbl + 1;

			if (!check_label(label) && pass < 2) {
				fprintf(err_file, "Line %d: Wrong label declaration\n", lineN);
				buffer = reset_buffer();
				continue;
			}
			buffer = trim(buffer);
			if (search_key(label) < 0 && pass > 1) {
				fprintf(err_file, "Line %d: Label was declared before\n", lineN);
			}
			else if (strstr(buffer, ".data")) {
				if (pass == 2)
					continue;
				read_int(label, buffer, err_file);
			}
			else if (strstr(buffer, ".string")) {
				if (pass == 2)
					continue;
				read_string(label, buffer, err_file);
			}
			else if (strstr(buffer, ".struct")) {
				if (pass == 2)
					continue;
				read_struct(label, buffer, err_file);
			}
			else if (pass == 1) {
				insert(label, IC + 100, 4);
				read_operation(buffer, err_file);
			}
			else
				read_operation(buffer, err_file);
		}
		else
			read_operation(buffer, err_file);
		buffer = reset_buffer();
	}

	if (pass == 1)
		IC_old = IC;

	free(as_fname);
	free(ext_fname);
	free(ent_fname);
	return 1;
}

int write_file(const char* fname, FILE *err_file)
{
	int i = 0;
	char buf[3];
	char buf1[3];
	FILE *fp;
	char* as_fname;

	pass = 0;
	as_fname = get_fname(fname, ".ob");

	fp = fopen(as_fname, "w");
	if (fp == NULL) {
		fprintf(err_file, "Cannot create file for writing\n");
		free(as_fname);
		return 0;
	}

	for (i = 0; i < IC; i++) {
		if (OperTable[i] == 0) {
			fprintf(err_file, "Variable used but none specified\n");
			fclose(fp);
			free(as_fname);
			return 0;
		}
		fprintf(fp, "%s\t%s\n", itom(i + 100, buf), itom(OperTable[i], buf1));
	}
	for (i = 0; i < DC; i++)
		fprintf(fp, "%s\t%s\n", itom(i + IC + 100, buf), itom(DataTable[i], buf1));

	free(as_fname);
	fclose(fp);
	return 1;
}