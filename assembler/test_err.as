; file test.as
mov "abs:sdf",r5
STR .string "qwe:123"
LENGTH: .data -1,-9,15,33,4,5,6,7
K: .data 3
S1: .struct 8, "abzxc34"
.entry LOOP
.entry LENGTH
.extern L3
.extern W
MAIN : mov S1.1, W
 add r2,STR
LOOP: jmp W
 prn #-5
 inc var
 sub r1, r4
 bne K
 W: .string "error"
mov S1.2, r3
 bne L3
END: stop
