#pragma warning(disable : 4996)
#include "hashtable.h"

struct DataItem {
	struct DataItem *next;
	int data; /* memory address */
	int type; /* 1 - Data, 2 - String, 3 - Struct, 4 - Operation, 5 - extern */
	char* key; /* label name */
};

struct DataItem* hashArray[SIZE];

unsigned hashCode(const char* key) {
	unsigned hashval;

	for (hashval = 1; *key != '\0'; key++)
		hashval = *key + 31 * hashval;
	return hashval % SIZE;
}

int search_key(const char* key) {
	/* get the hash */
	int hashIndex = 0;
	hashIndex = hashCode(key);

	/* move in array until an empty */
	while (hashArray[hashIndex] != NULL) {
		if (!strcmp(key, hashArray[hashIndex]->key))
			return hashArray[hashIndex]->data;
		++hashIndex;

		/* wrap around the table */
		hashIndex %= SIZE;
	}

	return -1;
}

int search_type(const char* key) {
	int hashIndex = 0;
	hashIndex = hashCode(key);

	while (hashArray[hashIndex] != NULL) {
		if (!strcmp(key, hashArray[hashIndex]->key))
			return hashArray[hashIndex]->type;

		++hashIndex;
		hashIndex %= SIZE;
	}

	return -1;
}

int search_key_ext(const char* key) {

	int i = 0;
	for (i = 0; i < SIZE; i++)
		if (hashArray[i] != NULL)
			if (!strcmp(key, hashArray[i]->key))
				return hashArray[i]->data;

	return -1;
}

int delete_key_ext(const char* key) {

	int i = 0;
	for (i = 0; i < SIZE; i++)
		if (hashArray[i] != NULL)
			if (!strcmp(key, hashArray[i]->key)) {
				free(hashArray[i]);
				hashArray[i] = NULL;
				return 1;
			}

	return -1;
}

int insert(const char* key, int data, int type) {

	int hashIndex = 0;
	struct DataItem *item = (struct DataItem*) malloc(sizeof(struct DataItem));
	if (item == NULL || key == NULL)
		return 0;

	hashIndex = hashCode(key);
	item->data = data;
	item->key = malloc(strlen(key) * sizeof(char));
	if (!item->key)
		return 0;
	strcpy(item->key, key);
	item->type = type;

	while (hashArray[hashIndex] != NULL) {
		++hashIndex;
		hashIndex %= SIZE;
	}

	hashArray[hashIndex] = item;
	return 1;
}

int update(const char* key, int data) {

	int hashIndex = 0;
	hashIndex = hashCode(key);
	while (hashArray[hashIndex] != NULL) {
		if (!strcmp(key, hashArray[hashIndex]->key)) {
			hashArray[hashIndex]->data = data;
			return 1;
		}
		++hashIndex;
		hashIndex %= SIZE;
	}

	return 0;
}

int delete(const char* key) {

	int hashIndex = 0;

	if (key == NULL) {
		while (hashIndex < SIZE) {
			free(hashArray[hashIndex]);
			hashArray[hashIndex] = NULL;
			++hashIndex;
		}
		return 1;
	}

	hashIndex = hashCode(key);

	while (hashArray[hashIndex] != NULL) {

		if (!strcmp(key, hashArray[hashIndex]->key)) {
			free(hashArray[hashIndex]);
			hashArray[hashIndex] = NULL;
			return 1;
		}
		++hashIndex;
		hashIndex %= SIZE;
	}

	return 0;
}
/*
void display() {
	int i = 0;

	for (i = 0; i<SIZE; i++) {

		if (hashArray[i] != NULL)
			printf("key: %s, data: %d, type: %d\n", hashArray[i]->key, hashArray[i]->data, hashArray[i]->type);
		else
			printf(" ~~ \n");
	}

	printf("\n");
}
*/