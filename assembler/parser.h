#pragma once
#ifndef ASM_CMD
	#define ASM_CMD {"mov","cmp","add","sub","not","clr","lea","inc","dec","jmp","bne","red","prn","jsr","rts","stop"}
#endif

#ifndef REGISTERS
	#define REGISTERS {"r0","r1","r2","r3","r4","r5","r6","r7"}
#endif

#ifndef REGISTER_NUM
	#define REGISTER_NUM 8
#endif

#ifndef ASM_CMD_NUM
	#define ASM_CMD_NUM 16
#endif

#ifndef STR_SIZE
	#define STR_SIZE 80
#endif

#ifndef LBL_SIZE
	#define LBL_SIZE 30
#endif

#ifndef MAX_DATA
	#define MAX_DATA 256
#endif

#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>

#include "hashtable.h"
#include "utils.h"

int read_file(const char* fname, FILE *err_file);
int read_struct(const char* label, char* line, FILE *err_file);
int read_string(const char* label, char* line, FILE *err_file);
int read_int(const char* label, char* line, FILE *err_file);
void read_operation(char* line, FILE *err_file);
int check_register(const char* reg);
int check_label(const char* label);
int read_var(char* var, const int type);
int get_var_type(const char* var);
int read_extern(const char* line, const char* ext_fname, FILE *err_file);
int read_entry(const char* entry, char* ent_fname, FILE *err_file);
int write_file(const char* fname, FILE *err_file);
void reset_globals(const char* fname);
char* reset_buffer();